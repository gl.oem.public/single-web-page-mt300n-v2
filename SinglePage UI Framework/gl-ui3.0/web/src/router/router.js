
"use strict";

define(["vue", "vueRouter", "require"], function (Vue, vueRouter, require) {
    Vue.use(vueRouter);
    var router = new vueRouter({
        routes: [{ path: "", redirect: "internet" }, {
            path: "/internet",
            name: "internet",
            component: function component(resolve) {
                require(["/src/temple/internet/index.js"], resolve);
            }
        }, {
            path: "/login",
            name: "login",
            component: function component(resolve) {
                require(["/src/temple/login/index.js"], resolve);
            }
        }, {
            path: "/process",
            name: "process",
            component: function component(resolve) {
                require(["/src/temple/process/index.js"], resolve);
            }
        }, {
            path: "/upgrade",
            name: "upgrade",
            component: function component(resolve) {
                require(["/src/temple/upgrade/index.js"], resolve);
            }
        }, {
            path: "/welcome",
            name: "welcome",
            component: function component(resolve) {
                require(["/src/temple/welcome/index.js"], resolve);
            }
        }]
    });
    return router;
});