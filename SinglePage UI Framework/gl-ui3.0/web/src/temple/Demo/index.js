"use strict";

define(["text!temple/Demo/index.html", "vue"], function (stpl, Vue) {
    var vueComponent = Vue.extend({
        template: stpl,
        data: function data() { },
        computed: {
            isconnectedData: function isconnectedData12() {
                return this.$store.getters.apiData['isconnected']
            }
        },
        mounted: function mounted() {
            this.$store.dispatch("call", { api: 'isconnected' }).then(function (result) {
                console.log(result)
                // result就是返回的数据,在此使用
            })
        },
        methods: {

        }
    });
    return vueComponent;
});