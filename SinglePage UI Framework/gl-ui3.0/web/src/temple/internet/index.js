"use strict";

define(["vue", "text!temple/internet/index.html", "css!temple/internet/index.css", "temple/wan/waninfo", "temple/wireless/index", "temple/clients/index", "temple/upgrade/index", "temple/lanip/index", "temple/adminpw/index", "temple/revert/index", "temple/Demo/index",], function (Vue, stpl, css, waninfo, wireless, client, upgrade, lanip, adminpw, revert, demo) {
    var vueComponent = Vue.extend({
        template: stpl,
        components: {
            "waninfo": waninfo,
            "wireless": wireless,
            "client": client,
            "upgrade": upgrade,
            "lanip": lanip,
            "adminpw": adminpw,
            "revert": revert,
            "demo": demo,
        },
        data() {
            return {
                list: ['waninfo', 'wireless', 'client', 'upgrade', 'lanip', 'adminpw', 'revert', 'demo']
            }
        },
        mounted: function mounted() {
            var that = this;
            $("#router-visual").slideDown();
            if ($(".clsLink2internet").hasClass("bar")) {
                $(".bar.active").removeClass("active");
                $(".clsLink2internet").addClass("active");
                $("#applications").collapse("hide");
                $("#moresetting").collapse("hide");
                $("#system").collapse("hide");
                $("#vpn").collapse("hide");
            };
        },
        methods: {}
    });
    return vueComponent;
});