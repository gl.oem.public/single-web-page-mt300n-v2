"use strict";

define(["text!temple/index/index.html", "vue", "component/gl-select/index", "component/gl-loading/index"], function (stpl, Vue, gl_select, gl_loading) {
    var vueComponent = Vue.extend({
        template: stpl,
        name: "app",
        data: function data() {
            return {
                appliedMessages: ["Reboot", "are you sure", "✔"],
                language: ["English", "简体中文", '繁体中文', "Deutsch", "Français", "Español"],
                webLang: 'EN',
                loading: true
            };
        },
        components: {
            "gl-select": gl_select,
            "gl-loading": gl_loading
        },
        computed: {
            // 首屏展示状态 全局循环调用
            router: function router() {
                return this.$store.getters.apiData["router"];
            },
            // 当前语言
            lan: function lan() {
                return this.$store.getters.lang;
            },
            // 弹框
            modal: function modal() {
                return this.$store.getters.modal;
            },
            // 语言栏显示当前语言
            lang: function lang() {
                var item = "English";
                switch (this.lan) {
                    case "CN":
                        item = "简体中文";
                        break;
                    case "EN":
                        item = "English";
                        break;
                    case "DE":
                        item = "Deutsch";
                        break;
                    case "FR":
                        item = "Français";
                        break;
                    case "SP":
                        item = "Español";
                        break;
                    case "TC":
                        item = "繁体中文";
                        break;
                }
                return item;
            },
            // 路由器wifi信息
            getaps: function getaps() {
                return this.$store.getters.apiData["getaps"];
            },
            // WAN口连接网络信息
            waninfo: function waninfo() {
                return this.$store.getters.apiData["waninfo"];
            },
            clients: function clients() {
                return this.$store.getters.apiData["getclients"];
            },
            // 通过wifi连接的设备数量
            wifiNum: function wifiNum() {
                var client = this.clients && this.clients.clients || []
                var len = client.length;
                var num = 0;
                for (var i = 0; i < len; i++) {
                    if (client[i].iface == '2.4G' || client[i].iface == '5G') {
                        num++;
                    }
                }
                return num
            },
            // 通过lan口连接的设备数量
            lanNum: function lanNum() {
                var client = this.clients && this.clients.clients || []
                var len = client.length;
                var num = 0;
                for (var i = 0; i < len; i++) {

                    if (client[i].iface == 'cable') {
                        num++;
                    }
                }
                return num
            },

            // 有无网络连接
            staActive: function staActive() {
                return this.$store.getters.apiData["internetreachable"]["reachable"] == null ? true : this.$store.getters.apiData["internetreachable"]["reachable"];
            },
            // 路由器型号图片
            appIcon: function appIcon() {
                if (this.router.model) {
                    var model = this.router.model;
                    if (model.toLowerCase().indexOf('usb') != -1) {
                        return "usb-router";
                    } else if (model == 'ar750') {
                        return "ar750-router";
                    } else if (model == 'ar750s') {
                        return "ar750s-router";
                    } else if (model.toLowerCase().indexOf('ar300m') != -1) {
                        return 'ar300m-router';
                    } else if (model.toLowerCase().indexOf('ar150') != -1) {
                        return 'ar300m-router';
                    } else if (model.toLowerCase().indexOf('x750') != -1) {
                        return 'x750-router';
                    } else if (model.toLowerCase().indexOf('mt300n-v2') != -1) {
                        return 'ar300m-router';
                    } else {
                        return "mini-router";
                    }
                }
            },
            phoneInter: function phoneInter() {
                if (this.router.model == 'usb150') {
                    if (this.wifiIconActive) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    if (this.waninfo.ip) {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            wanicon: function wanicon() {
                return this.waninfo.code != -17 && this.router.model != 'usb150';
            },
            noInter: function noInter() {
                return !this.waninfo.gateway && this.router.ip_addr !== this.waninfo.gateway && !this.staActive
            }
        },
        beforeCreate: function beforeCreate() {
            var that = this;
            that.$store.dispatch('call', { api: 'router' }).then(function (result) {
                that.loading = false;
                if (result.success) {
                    that.wifimode = result.mode;
                    if (result.mode !== 'router') {
                        that.$router.push('wireless');
                        // 非路由模式禁用
                    }
                }
            });
            // 首屏状态 全局循环调用
            setInterval(function () {
                that.$store.dispatch('call', { api: 'router' });
            }, 5000);
        },
        methods: {
            geclass: function geclass(name) {
                return "clsLink2" + name + " bar";
            },
            onShow: function onShow(animation) {
                this.modal.animation = animation;
                this.modal.show = true;
            },
            showHide: function showHide() {
                $("#ulLogo").toggleClass("ulShow");
                $("sub").toggle();
            },
            // modal取消按钮
            CancelFirm: function CancelFirm() {
                this.modal.cancel ? this.modal.cancel() : null;
                this.$store.commit("hideModal");
            },
            // modal确定按钮
            comfirm: function comfirm() {
                this.modal.cb ? this.modal.cb() : null;
                this.$store.commit("hideModal");
            },
            setlang: function setlang(data) {
                var lang = "";
                var that = this;
                switch (data) {
                    case "简体中文":
                        lang = "CN";
                        break;
                    case "English":
                        lang = "EN";
                        break;
                    case "Deutsch":
                        lang = "DE";
                        break;
                    case "Français":
                        lang = "FR";
                        break;
                    case "Español":
                        lang = "SP";
                        break;
                    case "繁体中文":
                        lang = "TC";
                        break;
                }
                this.webLang = lang;
                // 插件储存
                that.$translate.setLang(lang);
                // 后台储存
                this.$store.dispatch("call", {
                    api: "setlanguage", data: {
                        language: lang
                    }
                }).then(function (result) {
                    if (result.success) {
                        that.$store.commit("setLang", { lang: lang });
                    }
                });
            },
            logout: function logout() {
                var that = this;
                this.$store.dispatch('call', { api: "logout" }).then(function (result) {
                    if (result.success) {
                        window.location.href = "/login";
                    } else {
                        that.$message({
                            "type": "error",
                            "api": "logout",
                            "msg": result.code
                        });
                        return;
                    }
                });
            },
            reboot: function reboot() {
                this.$store.commit("showModal", {
                    show: true,
                    title: "Caution",
                    message: this.$lang.modal.isReboot,
                    cb: function cb() {
                        if (window.caniuse) {
                            sessionStorage.setItem("callfunction", "reboot");
                        }
                        window.location.href = "/process.html?action=reboot";
                    }
                });
            },
            userAgent: function userAgent() {
                var Sys = {};
                var ua = navigator.userAgent.toLowerCase();
                var state = null;
                var isOpera; (state = ua.match(/rv:([\d.]+)\) like gecko/)) ? Sys.ie = state[1] : (state = ua.match(/msie ([\d.]+)/)) ? Sys.ie = state[1] : (state = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = state[1] : (state = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = state[1] : (state = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = state[1] : 0;
                if (Sys.ie && parseInt(Sys.ie) <= 9) {
                    return true;
                }
                return false;
            }
        }
    });
    return vueComponent;
});