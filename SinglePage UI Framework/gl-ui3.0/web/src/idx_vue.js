'use strict';

require(['vue', 'vueX', 'router/router', 'store/store', 'lang', 'en', 'jquery', 'temple/index/index', 'component/gl-message/index', 'component/gl-modal/index', 'component/gl-loading/index', 'bootstrap'], function (Vue, Vuex, router, store, vuexI18n, en, jquery, indext, message, vodal, gl_loading) {
    window.Vue = Vue;
    Vue.component('vodal', vodal);
    Vue.prototype.$message = message;
    Vue.config.debug = true;
    Vue.config.devtools = true;
    Vue.use(vuexI18n);
    Vue.locales(en);
    Vue.directive('focus', {
        inserted: function inserted(el, _ref) {
            var value = _ref.value;
            if (value) {
                el.focus();
            }
        }
    });
    new Vue({
        el: '#app',
        router: router,
        store: store,
        components: {
            "gl-loading": gl_loading,
            'indext': indext
        },
        mounted: function mounted() {
            var that = this;
            that.$store.dispatch('call', { api: 'getlanguage' }).then(function (result) {
                if (result.success) {
                    that.$store.commit("setLang", { lang: result.language });
                    that.$translate.setLang(result.language);
                } else {
                    that.$store.commit("setLang", { lang: 'EN' });
                    that.$translate.setLang('EN');
                }
            });
            // 获取路由器wifi是什么
            this.$store.dispatch('call', { api: 'getaps' })
        }
    });
});