'use strict';

define(['vue', 'vueX', 'store/api', 'store/mock', 'jquery', 'language', 'promise', 'component/gl-message/index'], function (Vue, Vuex, api, mock, jquery, language, Promise, message) {
    var apiItem = {};
    Vue.prototype.$lang = language;
    var mode = 'develop'; // 开发模式
    // 不是浏览器访问修改为产品模式
    if (window.location.href.indexOf('chrome-extension') == -1) {
        mode = 'product';
    }
    var ip = '192.168.8.1'; // 当为开发模式时。将此处修改为路由器的host ip地址。
    var host = 'http://192.168.8.1';
    // 生成一个数据列表  键=API 值=数据
    if (Object.keys) {
        // 返回一个对象所有属性的数组
        Object.keys(api).map(function (item) {
            apiItem[item] = {};
        });
    } else {
        for (var key in api) {
            apiItem[key] = {};
        }
    }
    var state = {
        apiData: apiItem,
        lang: '',
        animation: ['zoom', 'fade', 'flip', 'door', 'rotate', 'slideUp', 'slideDown', 'slideLeft', 'slideRight'],
        modal: {
            show: false,
            animation: '',
            cb: null,
            cancel: null,
            esc: true,
            title: '',
            message: '',
            yes: '',
            no: '',
            type: "",
            messageTwo: '',
            messageThree: ''
        },
    };
    var getters = {
        apiData: function apiData(state) {
            return state.apiData;
        },
        modal: function modal(state) {
            return state.modal;
        },
        lang: function lang(state) {
            return state.lang;
        },

    };
    var actions = {
        // 全局方法 API调用
        call: function call(_ref, payload) {
            var state = _ref.state,
                commit = _ref.commit,
                datacache = false;
            var promise = new Promise(function (resolve) {
                var currentdate = new Date().getTime();
                if (payload.data) {
                    // 使用缓存
                    if (payload.cache) {
                        datacache = true;
                    }
                } else {
                    if (payload.sync) {
                        datacache = false;
                    } else {
                        datacache = true;
                    }
                }
                var hostip = payload.ip;
                var SyncTime = parseInt(currentdate - state.apiData[payload.api]['lastSync']);
                if (!SyncTime) {
                    SyncTime = 0;
                }
                // 如果是post请求不采用缓存策略。如果是get请求则使用缓存策略，在同一条时间线上不允许在3s内再次发起请求，对请求时间线进行清理。减少服务器数据的压力。
                if (datacache && state.apiData[payload.api]['lastSync'] && SyncTime < 3000 && payload.api.indexOf("status") != -1) {
                    // console.log("重复请求: ", payload.api, api[payload.api]);
                    resolve(state.apiData[payload.api]);
                } else {
                    // 记录请求的最后时间
                    commit('setSyncTime', { api: payload.api, lastSync: currentdate });
                    var contentType = payload.contentType;
                    var processData = payload.processData;
                    if (Object.prototype.toString.call(payload.data).split(' ')[1].split(']')[0] == 'FormData') {
                        contentType = false;
                        processData = false;
                    }
                    // ip没有传入
                    if (!hostip) {
                        // 产品模式直接通过域名访问api 开发模式通过192.168.8.1
                        hostip = mode == 'develop' ? host + api[payload.api] : api[payload.api];
                    } else {
                        hostip = 'http://' + payload.ip + api[payload.api];
                    }
                    // mock数据 
                    if (payload.mock) {
                        var mockData = {};
                        if (payload.mock == 'error') {
                            mockData.api = payload.api;
                            mockData.failed = true;
                            mockData.unsuccess = true;
                            mockData.errMsg = 'not data';
                            commit('setApiData', mockData);
                            resolve(state.apiData[payload.api]);
                        } else {
                            mockData = mock[payload.api];
                            mockData.api = payload.api;
                            mockData.lastSync = new Date().getTime();
                            if (mock[payload.api].code == 0) {
                                mockData.success = true;
                            } else {
                                mockData.success = false;
                            }
                            mockData.unsuccess = false;
                            commit('setApiData', mockData);
                            resolve(state.apiData[payload.api]);
                        }
                    } else {
                        jquery.ajax({
                            url: hostip,
                            data: payload.data,
                            type: payload.data == null ? 'get' : 'post', // data不存在为get请求
                            cache: false, // 不使用缓存 每次都会向服务器请求
                            dataType: payload.dataType == null ? 'json' : payload.dataType,
                            contentType: contentType,
                            processData: processData,
                            timeout: payload.timeOut == null ? 9000 : payload.timeOut,
                            success: function success(result) {
                                result.api = payload.api;
                                // 记录请求返回时间
                                result.lastSync = new Date().getTime();
                                // 更新store中的数据
                                commit('setApiData', result);
                                if (result.code == 0) {
                                    result.success = true;
                                } else {
                                    result.success = false;
                                }
                                if (result.token) {
                                    commit('setToken', result);
                                }
                                if (result.code == -1) {
                                    window.location.href = '/';
                                }
                                resolve(result);
                            },
                            error: function error(XMLHttpRequest, textStatus, errorThrown) {
                                var result = {};
                                if (textStatus == "timeout") {
                                    result.timeout = true;
                                    result.code = -6;
                                }
                                // console.log("Api Error： " + payload.api, '超时：' + result.timeout);
                                result.api = payload.api;
                                result.success = false;
                                result.failed = true;
                                resolve(result);
                            }
                        });
                    }
                }
            });
            return promise;
        },

    };

    // 暴露的全局同步方法
    var mutations = {
        // 将当前API请求的数据同步到state.apiData中
        setApiData: function setApiData(state, payload) {
            state.apiData[payload.api] = payload;
        },
        // 保存token
        setToken: function setToken(state, payload) {
            // state.token = payload.token
            window.localStorage.setItem('Token', payload.token);
        },
        setSyncTime: function setSyncTime(state, payload) {
            state.apiData[payload.api]['lastSync'] = payload.lastSync;
        },
        // 设置语言
        setLang: function setLang(state, payload) {
            state.lang = payload.lang;
        },
        changeVal: function changeVal(state, payload) {
            payload.data[payload.attr] = payload.val;
        },
        showModal: function showModal(state, payload) {
            state.modal.show = payload.show ? payload.show : true;
            state.modal.animation = payload.animation ? payload.animation : state.animation[parseInt(Math.random() * 10)];
            state.modal.title = payload.title ? payload.title : 'Caution';
            state.modal.yes = payload.yes ? payload.yes : 'Yes';
            state.modal.no = payload.no ? payload.no : 'No';
            state.modal.type = payload.type ? payload.type : 'default';
            state.modal.message = payload.message ? payload.message : 'message';
            state.modal.messageTwo = payload.messageTwo ? payload.messageTwo : '';
            state.modal.messageThree = payload.messageThree ? payload.messageThree : '';
            state.modal.esc = state.modal.esc ? state.modal.esc : true;
            state.modal.cb = payload.cb ? payload.cb : null;
            state.modal.cancel = payload.cancel ? payload.cancel : null;
        },
        hideModal: function hideModal(state) {
            state.modal.show = false;
            state.modal.animation = '';
            state.modal.message = '';
            state.modal.title = '';
            state.modal.cb = null;
            state.modal.cancel = null;
        }
    };
    Vue.use(Vuex);
    Vue.config.debug = true; // 1.为所有的警告打印栈追踪  2.把所有的锚节点以注释节点显示在 DOM 中，更易于检查渲染结果的结构。
    var store = new Vuex.Store({
        state: state,
        actions: actions,
        getters: getters,
        mutations: mutations,
        strict: false
    });
    return store;
});