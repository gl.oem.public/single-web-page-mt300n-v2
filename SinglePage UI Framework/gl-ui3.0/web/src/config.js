'use strict';

var version = '3.0.35';
require.config({
  urlArgs: version,
  baseUrl: '/src',
  waitSeconds: 200,
  paths: {
    'jquery': 'lib/jquery',
    'vue': 'lib/vue',
    'language': 'lib/lang/language',
    'vueRouter': 'lib/vue-router.min',
    'vueX': 'lib/vuex.min',
    'text': 'lib/text',
    'css': 'lib/css',
    'lang': 'lib/lang',
    'macaddress': 'lib/macaddress',
    'en': 'lib/lang/en',
    'apn': 'lib/apn-config',
    'promise': 'lib/promise.min',
    'polyfill': 'lib/polyfill.min',
    'bootstrap': 'lib/bootstrap.min',
    'jstz': 'lib/jstz.min',
  },
  shim: {
    'jquery': {
      exports: '$'
    },
    'bootstrap': {
      deps: ['jquery'],
      exports: '$.fn.bootStrap'
    },
    'vueX': {
      deps: ['polyfill', 'promise']
    }
  },
});
